plib.gui To Do List
===================

Planned Features
----------------

- Make sure that coverage is complete for common widget
  methods.

- Enforce consistent handling of class and instance variables
  (e.g., the ``_parent`` variable should be set in the same
  place for all widgets)--this applies particularly to the
  GTK toolkit (lots of cruft in there).

- Enforce common conventions for method and attribute names.

Future Features
---------------

These are features that might be implemented in future
versions of PLIB.GUI.

- Find a way to have the actual GUI widgets (from the chosen
  toolkit) take their docstrings from the base widgets (which
  define the API).

- Add find/replace functionality to ``PEditControl``.

- Add image and movie display widgets to ``plib.gui``.

- Add an HTML browser widget to ``plib.gui``.

- Is tri-state check box support really needed? Or
  can it be factored out into a separate class
  (``PTriStateCheckBox``)?

- Add center/right justify options to ``PTextLabel``
  (maybe also vertical alignment options).

- The status bar "custom widgets" may be too much;
  may only need to allow more text fields after the
  first, with some way of getting/setting the text.
  Also, add an example program that demonstrates the
  status bar functionality.

- Add a ``WEB`` (or ``WWW``) GUI toolkit that allows the
  same declarative method of specifying a GUI to be used
  in web applications.

Unplanned Features
------------------

These are features that were planned at one point but have
been dropped, unless somebody can convince me that they're
actually worth doing. If I ever do them, it will be because
I have a *lot* of spare time. :-)

- Add Win32 (using the PyWin32 extensions) to the list
  of toolkits supported by ``plib.gui``.

- Add Cocoa (Mac OS X GUI framework) to the list of
  toolkits supported by ``plib.gui``.

Bugs/Workaround Fixes
---------------------

- In KDE 3, displaying dialogs sometimes causes a segfault
  when there is a NotifierClient in use; other than that, what
  conditions trigger the segfault are not entirely clear, nor
  are they entirely consistent (e.g., for a while I saw them
  on my KDE 3.5.10 machine, but not on a KDE 3.5.5 one; but
  after some more code adjustments the pattern reversed???),
  so no workaround is implemented yet. The cause is probably
  some interaction between the dialog's local event loop and
  the socket notifier, since the segfault never appears if
  there is no notifier in use.

- In KDE 3, a segfault can also be caused by displaying the
  about or about toolkit dialog, but only if the dialog is
  triggered by a toolbar action, and only in some applications.
  For example, the ``pyidserver-gui`` example program does not
  appear to ever trigger it (it launches the dialogs from action
  buttons and has no menu or toolbar); the ``gui-signals`` example
  program sometimes triggers it, depending on the particular
  machine (it launches the dialogs using toolbar actions and has
  no menu, just a toolbar); the ``scrips-edit`` example program
  triggers it more reliably, but only if launched using toolbar
  actions; if launched from menus, the dialogs display OK with no
  segfault, and if the toolbar is *then* used to launch them in
  the same program run, they work fine!

- In later KDE 3 and Qt 3 versions (KDE 3.5.10, Qt 3.3.8 on the
  machine where I've observed this), the mechanisms for adding
  toolbar separators that work in earlier versions appear to
  no longer work. (Note: This is not always observed; it seems
  to depend on the Linux distribution, so it may be a bug in
  some distros' Qt packages.)

- Setting a font on a group box automatically changes the fonts
  on all its child widgets; need to override implementation to
  only set the font on the groupbox title.

- Embedding a list view in a group box is buggy in Gtk and Wx;
  the list view spills over the group box border.

- Setting font properties doesn't work in Gtk and Wx.

- The group box API is inconsistent; a normal groupbox has a
  ``_controls`` private attribute that's a list, while an auto
  group box that initializes itself from specs has a dict for
  that private attribute.

- Need to add a field to panels to keep a list of child widgets.

- The Qt 3 list view widget header column alignment doesn't
  work; there doesn't appear to be any API to do it. The table
  widget column alignment also doesn't work in Qt 3, for either
  the header *or* the widget itself; again, there doesn't seem
  to be any API to do it.

- There appears to be no way to get an entire row of the Qt 3
  table widget to repaint itself when the text color changes
  (it repaints fine when the background color changes).

- In Qt 4 on Windows, it's even weirder: the text color change
  repaints fine, and so does the background color change,
  *except* for the first table row! ???

- Read-only widgets are still in the tab order in Qt/KDE
  (disabled widgets aren't, but the point of read-only widgets
  is to not have user input but look normal, not greyed out).

- Setting background color doesn't appear to work correctly for
  all Qt/KDE 4 widgets.

- Some KDE standard actions don't appear to behave
  consistently with the documentation.

- The wxWidgets "stock" item behavior seems inconsistent;
  the stock item doesn't always override a custom caption
  and image for menu and toolbar items, even though it
  should based on comparison with the corresponding GTK
  code.

- The wx list view column alignment doesn't work (there is an
  API function but it appears to be broken).

- The wx table column alignment doesn't work (there appears
  to be an API function but no documentation about what the
  alignment arguments should be; experimentation has not yet
  figured this out).

- The wxWidgets tab widget is firing a tab changed
  event on shutdown, after the status bar has been
  deleted (causes exception in ``pxmlview.py``--currently
  a hack is being used to mask this).

- The wx window background colors seem all messed up with
  certain color schemes in effect.

- The wx and GTK edit controls don't appear to have APIs for
  some of the desired functionality in the ``pnotepad``
  example program.

- The tab key doesn't move between controls in wx and GTK.

- The wx and GTK table widgets don't appear to give good sizing
  information for a main window to wrap to them as a client.

- The GTK top window doesn't set its coordinates in the
  settings file correctly; it always returns 0, 0 for its
  left, top coordinates. (GTK in general seems to make it
  very fscking difficult to get information that would seem
  pretty basic and is easy to get in other toolkits.)

- The GTK message dialog doesn't work correctly
  with OK/Cancel buttons; both buttons appear to
  return the "OK" response.

- The GTK file open/save dialogs won't open; need
  bug fix (parent window of dialog seems incorrect).

- The current GUI layout code does not support setting
  different margins for top/bottom vs. left/right sides
  of a panel; this is because there is no easy way to
  do this in GTK. As a result, currently if margins are
  not set all around, buttons and combos in GTK will
  expand to the edge of their containing panel, which is
  usually not what is wanted. The only workarounds are
  to use margins on all sides (meaning a little extra
  spacing at the edges of panels that contain buttons
  and combos) or to enclose each individual button and
  combo in its own panel (which is messy).

- The GTK list view column alignment and column auto-sizing
  doesn't work.

- The GTK table widget is much too crude. If I can figure
  out workarounds for the quirks of wxWidgets when using
  GTK under the hood, this issue will probably be what
  drives me to drop direct GTK support in ``PLIB.GUI``.
