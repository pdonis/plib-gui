#!/usr/bin/env python
"""
Sub-Package GUI.TOOLKITS of Package PLIB -- Python GUI Toolkits
Copyright (C) 2008-2015 by Peter A. Donis

Released under the GNU General Public License, Version 2
See the LICENSE and README files for more information

This sub-package contains sub-sub-packages for each of the supported
GUI toolkits.
"""
