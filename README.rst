plib.gui
========

The PLIB.GUI package contains a simple GUI application framework
with two main features:

- It lets the same high-level code work with a number of
  different underlying GUI toolkits. Currently supported:
  Qt (versions 3, 4, and 5), PySide and PySide 2, KDE
  (versions 3 and 4), wxWidgets, and GTK. (The original
  reason for writing this sub-package was that wxWidgets
  doesn't use Qt and I like the Qt/KDE widgets better, but
  Qt was not available for Windows and I wanted code that
  would run cross-platform.)

- It allows you to express the layout of your GUI in terms
  of Python lists and dicts, enabling a much more declarative
  and easy to read (and maintain) coding style.

Other than selecting the toolkit (which may not be necessary:
the main module of the sub-package can 'auto-detect' which
toolkit to use--the ``plib-setup-gui`` post-install script
does most of the work to enable this--so you only need to
override if you don't like the default), you should not have
to worry about any toolkit internal details; the goal of this
sub-package is to make them all look the same to your code.

Note that the GTK toolkit support in this sub-package is
"experimental" and may be removed if it proves to be more
trouble than it's worth. It's currently included because
wxWidgets' behavior when using GTK as its underlying GUI
framework has some quirks that I haven't been able to work
around yet. However, the GTK implementation of a number of
widgets (particularly tables and list/tree views) is much
less capable than the wxWidgets one, so the Python code for
GTK ends up relying much more on ugly hacks.

Note: PLIB.GUI works with Python 2.7. If you are using
Python 3, see the PLIB3.GUI package, available at
https://gitlab.com/pdonis/plib3-gui.

The ``setup.py`` script for PLIB.GUI uses the ``setuputils``
helper module, which helps to automate away much of the
boilerplate in Python setup scripts. This module is available
as a separate release at https://gitlab.com/pdonis/setuputils.

Installation
------------

To install PLIB.GUI, you can simply run::

    $ python setup.py install

at a shell prompt from the directory into which you
unzipped the source tarball (the same directory that this
README file is in). This will install PLIB and then
run each of the post-install scripts in the scripts
directory.

Example Programs
----------------

PLIB.GUI comes with example programs that illustrate key features
of the package. After installation, these can be found in the
``$PREFIX/share/plib/examples`` directory. If you have a
POSIX system (Linux or Mac OSX), the ``plib-setup-examples``
post-install script will install symlinks to the example
programs in the ``$PREFIX/bin`` directory.

The Zen of PLIB
---------------

There is no single unifying purpose or theme to PLIB, but
like Python itself, it does have a 'Zen' of sorts:

- Express everything possible in terms of built-in Python
  data structures.

- Once you've expressed it that way, what the code is
  going to do with it should be obvious.

- Avoid boilerplate code, *and* boilerplate data. Every
  piece of data your program needs should have one and
  only one source.

Copyright and License
---------------------

PLIB.GUI is Copyright (C) 2008-2019 by Peter A. Donis.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version. (See the LICENSE file for a
copy of version 2 of the License.)

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
